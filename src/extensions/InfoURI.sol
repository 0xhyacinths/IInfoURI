// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "openzeppelin-contracts/contracts/utils/introspection/ERC165.sol";
import "../interfaces/IInfoURI.sol";

/**
 * @dev Interface for Self-Describing Contracts
 *
 * A standardized way to provide a metadata URI for the contract as a whole.
 */
abstract contract InfoURI is IInfoURI, ERC165 {
    string private _info_uri;

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override(IERC165, ERC165)
        returns (bool)
    {
        return
            interfaceId == type(IInfoURI).interfaceId ||
            super.supportsInterface(interfaceId);
    }

    /**
     * @dev Returns the contract's informational metadata URI.
     */
    function infoURI() external view returns (string memory) {
        return _info_uri;
    }

    /**
     * @dev Sets the contract's informational metadata URI.
     */
    function _setinfoURI(string memory _new_info_uri) internal virtual {
        _info_uri = _new_info_uri;
    }
}
