// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "openzeppelin-contracts/contracts/utils/introspection/IERC165.sol";

/**
 * @dev Interface for Self-Describing Contracts
 *
 * A standardized way to provide a metadata URI for the contract as a whole.
 */
interface IInfoURI is IERC165 {
    /**
     * @dev Returns the contract's metadata URI.
     */
    function infoURI() external view returns (string memory);
}
