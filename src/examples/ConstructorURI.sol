// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "../extensions/InfoURI.sol";

contract ConstructorURI is InfoURI {
    constructor(string memory _meta_uri) {
        _setinfoURI(_meta_uri);
    }
}
