// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "../extensions/InfoURI.sol";

contract StaticURI is InfoURI {
    constructor() {
        _setinfoURI("http://example.com/static_metadata.json");
    }
}
