// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "../extensions/InfoURI.sol";
import "openzeppelin-contracts/contracts/access/Ownable.sol";

contract DynamicURI is InfoURI, Ownable {
    constructor(string memory _meta_uri) {
        _setinfoURI(_meta_uri);
    }

    function setInfoURI(string memory _meta_uri) public onlyOwner {
        _setinfoURI(_meta_uri);
    }
}
