// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import "forge-std/Test.sol";
import "../src/examples/ConstructorURI.sol";

contract ConstructorURITest is Test {
  ConstructorURI inst;

  function setUp() public {
    inst = new ConstructorURI("http://example.com/static_metadata.json");
  }

  function testConstructorURI() public {
    assertEq(inst.infoURI(), "http://example.com/static_metadata.json");
  }

  function testConstructorURISupportsInterface() public {
    assertTrue(inst.supportsInterface(0xbea80038));
  }
}
