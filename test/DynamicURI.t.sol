// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import "forge-std/Test.sol";
import "../src/examples/DynamicURI.sol";
import "../src/interfaces/IInfoURI.sol";

contract DynamicURITest is Test {
  DynamicURI inst;

  function setUp() public {
    inst = new DynamicURI("http://example.com/static_metadata.json");
  }

  function testDynamicURI() public {
    assertEq(inst.infoURI(), "http://example.com/static_metadata.json");
    inst.setInfoURI("http://example.org/static_metadata.json");
    assertEq(inst.infoURI(), "http://example.org/static_metadata.json");
  }

  function testDynamicURISupportsInterface() public {
    assertTrue(inst.supportsInterface(0xbea80038));
  }
}
