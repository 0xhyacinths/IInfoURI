// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import "forge-std/Test.sol";
import "../src/examples/StaticURI.sol";

contract StaticURITest is Test {
  StaticURI inst;

  function setUp() public {
    inst = new StaticURI();
  }

  function testStaticURI() public {
    assertEq(inst.infoURI(), "http://example.com/static_metadata.json");
  }

  function testStaticURISupportsInterface() public {
    assertTrue(inst.supportsInterface(0xbea80038));
  }
}
