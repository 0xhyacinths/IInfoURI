# IInfoURI
## Summary
Creates a standard method to retrieve smart contract metadata.

## Abstract
With this, we standardize the `metadataURI` method. Additionally, we define
specifics on how this feature can be discovered and used by indexers.

## Motivation
It is currently difficult to identify the source code of deployed contracts in a
manner that is consistent with the emphasis on decentralization. There exist
some solutions, however they are generally unsatisfactory in the way they are
implemented.

## Specification
We define an interface, `IInfoURI` that returns a string:
```solidity
pragma solidity ^0.8.0;

// IInfoURI has the ERC-165 interface ID 0xbea80038
interface IInfoURI is IERC165 {
  function infoURI() external view returns (string memory);
}
```
Calling this function will return a URI referencing the contract's metadata. The
specifics on this format are TBD.

## Rationale
While the Solidity compiler attempts to insert this metadata into the end of a
contract's bytecode, the specifics on how that metadata hash is generated are
difficult to reproduce and are heavily dependent on precise filesystem layout.

## Implementation
```solidity
pragma solidity ^0.8.0;

import "./interfaces/IInfoURI.sol";

contract ExampleURI is IInfoURI {
  function infoURI() external view returns (string memory) {
    return "http://example.com/static_metadata.json";
  }

  function supportsInterface(bytes4 interfaceId) public view returns (bool) {
    return interfaceId == type(IInfoURI).interfaceId;
  }
}
```


[1]: https://eips.ethereum.org/EIPS/eip-165
